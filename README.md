Guild Wars 2 TacO on Linux
==========================

Description
-----------

This is a project providing a few scripts & workarounds to be able to run gw2taco on linux under different distributions & desktop environments. This is a workaround to the Aero theme's composition/transparency effects that are required by TacO to run properly.

When launching GW2TacO on Linux (using wine) you encounter a black background and cannot see GW2. This is due to TacO utilizing the Desktop Window Manager API (dwmapi) and the Aero theme's composition/transparency effects which does not work in Linux via wine.

The gw2taco_linux module uses Linux's own window desktop managers & compositors as a workaround to various issues.

Different desktop environments on Linux come with different Window Managers/Compositors. Therefore the workaround will be slightly different depending on your distribution. If your distributions native compositor provides active-opacity then it's just a matter of changing the opacity level. However if the compositor lacks this feature then we install Picom (a great lightweight compositor) as your next best option.

The provided module attempts to make this process easier for a wide variety of configurations and easy integration into ArmordVehicle's Portable Performance Package.

Overview Video
---------------

[![GWTacO on Linux Youtube Vid](http://img.youtube.com/vi/NVk6ij7eZBI/0.jpg)](https://www.youtube.com/watch?v=NVk6ij7eZBI "GWTacO Linux v1.0.0 (Beta)")

Download / Using the Package
-----------------------------

**NOTE:** If you are not comfortable with the linux terminal/troubleshooting, then rather wait for a "stable" version of the module. The current version is in beta testing and some caution is advised.

**NOTE 2:** Please do a fresh install when upgrading versions (run install_taco.sh and choose option 1). At the moment there is no easy way to transfer the necessary tweaks & configs that get altered in between versions.

**1)** Download package:<br>
    -> [Latest Release (v1.1.1) (Beta)](https://io.namo.ninja/os/gw2taco_linux/gw2taco_linux_v1.1.1.tar.gz) MD5: **c978b62913db3cea66c59c50219b31b3**<br>
    -> [Semi-stable Release (v0.9a) (Alpha)](https://io.namo.ninja/os/gw2taco_linux/gw2taco_linux_v0.9a.tar.gz) MD5: **e957b7f3eb414a51aa7dedce37c2626d**<br>
**2)** Extract the archive in the root of your Guild Wars 2 folder.<br>
**3)** Navigate to the gw2taco folder and run install_taco.sh<br>
**4)** Use run_taco.sh after GW2 starts, or if autostart=true, then use run_taco.sh to start GW2 & Taco.<br>

**TacoPacks** - If you would like to use one of the other Taco packs (not the official) then choose a package from the "TacoMan". You can also provide your own custom download link and install the package via TacoMan.

**Lutris** - Make sure to extract the package to the right location, where the GW2.exe resides. This will be ../YourLutrisGW2folder/drive_c/Program Files/Guild Wars 2.<br>

If you want to use autostart, then change gameid=0 in config/settings.conf to the number in lutris for gw2. This can be found by creating a shortcut on desktop. Use run_taco.sh and it will start Gw2 via lutris, followed by taco. (This is manual, as I have not found any config to automate getting the gameid which differs depending on how many you have installed in lutris.)

Features
--------

* Installer/Manager (TacoMan)<br>
    -> Download/Install Official GW2TacO from the UI<br>
    -> Download/Install other GW2TacO packs from the UI<br>
    -> Able to have multiple tacopacks installed and switch between them easily<br>
    -> Package info & URLs are stored in a config file for easy manipulation<br>
    -> Hooks into ArmordVehicle's portable package wine binaries and can autostart with GW2.<br>
    -> Can use portable package, lutris or native WINE binaries.<br>
* Runtime (TacoRunner)<br>
    -> Toggles compositor settings for you on run and resets your defaults on exit.<br>
    -> A settings.conf file for changing opacity/autostart/startup settings easily.<br>
* Supported Distributions:<br>
    -> Debian/Ubuntu/Mint<br>
    -> Arch/Manjaro<br>
* Supported Desktop Environments<br>
    -> KDE, XFCE, CINNAMON, GNOME<br>
* Other<br>
    -> Lutris support<br>
    -> Opacity mode<br>
    -> Transparency mode

**NOTE:** As of **v1.0.0 gw2taco_linux** supports 2 modes.<br>
* **Transparency** = Experimental, but allows for full transparency. Expects bugs/performance issues and undesired effects!<br>
* **Opacity** = More stable but dimished clarity/brightness. Usefull as a fallback if transparency mode gives any issues.

FAQ / Troubleshooting
---------------------

The general rule of thumb is that, if you have an issue not being able to move or click or something, a combination of Alt-Tab, Minimizing-Maximizing and Fullscreen, usually solves the problem.

Your safest option is to leave **autostart=false** and start GW2 manually. Log in, select character and load the map.

It seems that GW2 in fullscreen has less issues OR are easier to overcome.<br>

**MODE = OPACITY**
1) **NoTaco** (only GW2 shows)<br>
--> TacO failed to start<br>
--> Compositor failed to start & GW2 is on top of taco<br>
--> Active-opacity is set to 1 & GW2 is on top of taco<br>
2) **BlackTaco** (TacO loads with a black background, can't see GW2)<br>
--> TacO is on top of GW2 (Set the GW2 window to "Always on Top")<br>
3) **TroublesomeTaco** (Can see GW2 & TacO, Can Move but Can't Click)<br>
--> TacO is on top of GW2? (try Alt-Tab/Switching screens)<br>
4) **TroublesomeTaco** (Can see GW2 & TacO, Can't Move & Can't Click)<br>
--> The window focus of TacO & GW2 is rapidly alternating, creating randomness, audio issues & preventing the use of mouse/skills. User input becomes RNG. (Try minimizing the game then maximizing |OR| set game to Fullscreen.)<br>

**MODE = TRANSPARENCY**

Once the map is loaded, Alt-Tab -> ./run_taco.sh -> Alt-Tab back to GW2.

Wait for GW2Taco to load, it will be black for a few seconds, then it should go transparent. Once this happens, try moving camera, character and clicking around. Most of the time your controls will be somewhat locked.<br>

Try (in order):

* Alt-Tab to Guild Wars 2 Tactical Overlay window -> Click/Move around a bit to see if it settles in or issues continue. Repeat
* Alt-Tab to GW2 windows then back to Taco window -> Click/Move around a bit to see if it settles in or issues continue.
* GW2 Options -> Minimize -> Maximize -> Alt-tab a bit
* GW2 Options -> WindowMode -> Fullscreen -> Alt-tab a bit

**ROUTES / TRAILS not showing**

* Use winetricks to install d3dcompiler_43.dll

**GENERAL**

* When TACOwindow is not available in Alt-Tab (tested/appears in cinnamon), change (native settings to show child windows) OR (experiment with Switching/Minizing windows) OR (wait for a future release where we try to patch this automatically)

* Transparency requires TACO to be ontop of GW2. Do not force any raising or lowering of windows as we do with the Opacity mode. (leave as default)<br>

* It might be good to assign a keyboard shortcut binding, to execute run_taco.sh while ingame. This should help the situation. You can further use the binding to start GW2 & Taco by keybind if autostart=true.

* If using **autostart=true**, leave the StartTimeDelay quite long, to ensure that you login and load before the script starts taking over. Current recommended default is (120s)

Explanation
-----------

When using **MODE=Opacity** we can workaround the issue with diminished clarity.

The idea is that by setting our active-window to always-be-above than the inactive one(below) we can see through it (with opacity<100%). Setting TacO to always be below (inactive-window) and GW2 always on top (active-window), ensures that the GW2 Window takes input while we see the (inactive) TACO window below

When using **MODE=Transparency** we slightly change our approach.

By using fake-transparency, we are able to remove a certain RGBA color from the GW2TACO window. In this case we remove rgba(0,0,0,1) (solid black) from TACOwindow.

We now have to restore default behaviour, by *leaving* the GW2Taco window to always be on top of GW2.

However, the transparent zone does not redirect keyboard/mouse input  to GW2window because TACOwindow is a solid window with fake transparency. We can see through it... but we cannot interact with what's below it...

Therefore we use xdotool to redirect input from TACOwindow to GW2window while maintaining the TACO window presence.

Configurations
--------------
If you want to adjust settings look in the config folder

*placeholders for future info*
* General (settings.conf)<br>
--> <br>

* Compositor (picom.conf)<br>
--> <br>

* URLs (taco.links)<br>
--> <br>

Links
-----

* [GW2TacO website](http://www.gw2taco.com/)
* [gw2taco_linux GW2forum post](https://en-forum.guildwars2.com/discussion/comment/1275884/#Comment_1275884)
* [ArmoredVehicle's portable GW2 package for Linux](https://en-forum.guildwars2.com/discussion/31192/playing-guild-wars-2-on-linux-performance-optimizations-and-more)
