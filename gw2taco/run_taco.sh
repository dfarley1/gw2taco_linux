#!/bin/bash

# ========================= DESCRIPTION ====================================#
# Description: Runtime part of the gw2taco_linux package
# Project Link: https://gitlab.com/namoninja/gw2taco_linux
# Author: Namo Ninja
# License: GNU GPL v3+
# Version: 1.1.1
# ==========================================================================#

# 	To minimize repetitive code, the script currently assumes
# 	that Debian and Arch distributions use identical commands 
#	to interact with the DE's settings and that Cinnamon & GNOME
#   require an identical tweaks.

# ============================ VARS ========================================#
# Identify location of where script is located and !not where it started
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

# Identify where script is starting from when using portable package
GW=`pwd | grep -o 'drive_c/GW2'`
if [ -z $GW ]; then
	GW="gw2taco/app"
fi
cd "$DIR"

# Collect config values
SETTINGS="config/settings.conf"

if [ -f ${SETTINGS} ]; then
	GDIR=`grep 'gw2dir=' ${SETTINGS} | sed 's/^.*=//'`
	TDIR=`grep 'tacodir=' ${SETTINGS} | sed 's/^.*=//'`
	DE=`grep 'desktop=' ${SETTINGS} | sed 's/^.*=//'`
	DIST=`grep 'distribution=' ${SETTINGS} | sed 's/^.*=//'`
	STA=`grep 'autostarttime=' ${SETTINGS} | sed 's/^.*=//'`
	STM=`grep 'manualstarttime=' ${SETTINGS} | sed 's/^.*=//'`
	COUNT=`grep 'kwinrule=' ${SETTINGS} | sed 's/^.*=//'`
	WINE=`grep 'wine=' ${SETTINGS} | sed 's/^.*=//'`
	AUTO=`grep 'autostart=' ${SETTINGS} | sed 's/^.*=//'`
	OPACITY=`grep 'opacity=' ${SETTINGS} | sed 's/^.*=//'`
	ACTIVE=`grep 'active=' ${SETTINGS} | sed 's/^.*=//'`
	MODE=`grep 'mode=' ${SETTINGS} | sed 's/^.*=//'`
	PICOM="${TDIR}/config/picom.conf"
	GLSL="${TDIR}/config/transparency.glsl"

	if [ ${WINE} == "lutris" ]; then
		LUTG=`grep 'gameid' ${SETTINGS} | sed 's/^.*=//'`
		LUTP=`grep 'prefix' $HOME/.config/lutris/games/guild-wars-2*.yml | sed 's/^.*:.//'`
		LUTV=`grep 'version' $HOME/.config/lutris/games/guild-wars-2*.yml | sed 's/^.*:.//'`
		LUTW="$HOME/.local/share/lutris/runners/wine/${LUTV}/bin"
	fi

else
	printf "ERROR: Could not find ${SETTINGS} and cannot start without it. Exiting..."
	exit 2
fi

# Define Colors
GC='\e[32m' # Green
RC='\e[91m'	# Red
YC='\e[93m'	# Yellow
CC='\e[96m' # Cyan
NC='\e[0m'	# No color

#=========================== FUNCTIONS ==============================
#-------------------------- ERROR CHECKS----------------------------#
check_error()
{
	local check="$1"
	local exitcode="$2"
	local window_id="$2"
	local window_name="$3"

	case $check in

		"status")
			if [ ${exitcode} -ne 0 ]; then
				printf "${RC}---> ERROR! [err:${NC}${exitcode}${RC}]${NC}\n\v"
			else
				printf "${GC}---> OK!${NC}\n\v"
			fi
		;;

		"window_id")
			if [ ${window_name} == "GW2" ]; then
				if [ -z ${window_id} ]; then
					printf "\n${RC}:::WINDOW_ID_GW2=${YC}NULL${GC}"
				else
					printf "\n${GC}:::WINDOW_ID_GW2=${YC}${window_id}${GC}"
				fi

			elif [ ${window_name} == "TACO" ]; then
				if [ -z ${window_id} ]; then
					printf "\n${RC}:::WINDOW_ID_TACO=${YC}NULL${NC}\n"
				else
					printf "\n${GC}:::WINDOW_ID_TACO=${YC}${window_id}${NC}\n"
				fi
			fi
		;;

	esac
}

#------------------------------- xfwm4 -----------------------------#
do_xfwm4()
{
	local action="$1"

	if [ ${action} == "disable_compositor" ]; then

		printf "${GC}\n\v*** DISABLING XFWM4 COMPOSITION ***${NC}"
		xfconf-query -c xfwm4 -p /general/use_compositing -s false
			check_error "status" "$?"
	
	elif [ ${action} == "start_compositor" ]; then

		printf "${GC}\n\v*** RESTORING ${CC}xfwm4${GC} COMPOSITION ***${NC}"
		xfconf-query -c xfwm4 -p /general/use_compositing -s true
			check_error "status" "$?"

	else
	exit 2
	fi

}

#------------------------------ TWEAKS -----------------------------#
do_tweaks()
{
	local action="$1"

	# Collect window IDs by matching window names(title)
	if [ ${action} == "start" ]; then
		GW2window=`xdotool search --name '^Guild Wars 2$'`
			check_error "window_id" "${GW2window}" "GW2"
		TACOwindow=`xdotool search --name '^Guild Wars 2 Tactical Overlay$'`
			check_error "window_id" "${TACOwindow}"	"TACO"
	fi

	# Redirect GNOME to Cinnamon Tweak
	if [ "${DE}" == "GNOME" ]; then
		DE="Cinnamon"
	fi

	case "$DE" in

		XFCE) #======================== XFCE =============================#

			#------------------------------ DEBIAN & ARCH ------------------------------#
			if [ "$DIST" == "Debian/Ubuntu/Mint" ] || [ "$DIST" == "Arch/Manjaro" ]; then
			#------------------------------ DEBIAN & ARCH ------------------------------#

				#---------- TRANSPARENCY MODE ---------#
				if [ "${MODE}" == "transparency" ]; then
				#---------- TRANSPARENCY MODE ---------#

					#------------ ACTIVATE ------------#
					if [ "$action" == "start" ]; then
					#------------ ACTIVATE ------------#

						do_xfwm4 "disable_compositor"
						sleep 2 #safety delay

						printf "${GC}\n*** STARTING ${CC}Picom${GC} in ${YC}TRANSPARENCY${GC} mode ***\n${NC}"
						picom --config ${PICOM} --backend glx --glx-fshader-win "$(cat ${GLSL})" & PID_PICOM=$!
						sleep 2 #safety delay
							printf "${GC}:::PID_PICOM=${YC}${PID_PICOM}${NC}\n"

						sleep 2 #safety delay
						printf "${GC}\n*** SETTING TRANSPARENCY PARAMETERS ***\n${NC}"
						picom-trans -w "${TACOwindow}" 99
							check_error "status" "$?"
						
						printf "${GC}\n*** REDIRECTING WINDOW FOCUS ***\n${NC}" 
						sleep 2 #safety delay
						xdotool behave ${TACOwindow} focus windowfocus ${GW2window} & PID_xdotool=$!
							printf "${GC}:::PID_xdotool=${YC}${PID_xdotool}${NC}\n"

					#<<<---------- DISABLE ---------->>>#
					elif [ "$action" == "stop" ]; then
					#<<<---------- DISABLE ---------->>>#

						printf "${GC}\n\v*** DISABLING THE ${CC}Picom${GC} COMPOSITOR ***${NC}"
						kill `pidof picom`
						check_error "status" "$?"

						do_xfwm4 "start_compositor"

					else
					exit 2
					fi # finish action

				#----------- OPACITY MODE ----------#
				elif [ "${MODE}" == "opacity" ]; then
				#----------- OPACITY MODE ----------#

					#------------ ACTIVATE ------------#
					if [ "$action" == "start" ]; then
					#------------ ACTIVATE ------------#

						do_xfwm4 "disable_compositor"
						sleep 2 #safety delay

						printf "${GC}\n\v*** STARTING THE picom COMPOSITOR ***${NC}"
						picom --config ${PICOM} &
						
						printf "${GC}*** SETTING OPACITY PARAMETERS. ${YC}active-opacity${GC} IS AT ${YC}${OPACITY}${GC} ***${NC}"
						picom-trans -w "${GW2window}" ${OPACITY}
							check_error "status" "$?"

					#<<<---------- DISABLE ---------->>>#
					elif [ "$action" == "stop" ]; then
					#<<<---------- DISABLE ---------->>>#

						printf "${GC}\n\v*** DISABLING THE picom COMPOSITOR ***${NC}"
						kill `pidof picom`
							check_error "status" "$?"

						do_xfwm4 "start_compositor"

					else
					exit 2
					fi # finish action
				
				else
				exit 2
				fi # finish mode

			else
			exit 2
			fi # finish distribution
			
		;; #========================= END XFCE ===========================#

		KDE) #========================= KDE ==============================#

			#------------------------------ DEBIAN & ARCH ------------------------------#
			if [ "$DIST" == "Debian/Ubuntu/Mint" ] || [ "$DIST" == "Arch/Manjaro" ]; then
			#------------------------------ DEBIAN & ARCH ------------------------------#

				#---------- TRANSPARENCY MODE ---------#
				if [ "${MODE}" == "transparency" ]; then
				#---------- TRANSPARENCY MODE ---------#

					#------------ ACTIVATE ------------#
					if [ "$action" == "start" ]; then
					#------------ ACTIVATE ------------#

						printf "${GC}\n*** DISABLING ${CC}KWin${GC} ***\n${NC}"
						qdbus org.kde.KWin /Compositor suspend
							check_error "status" "$?"
						qdbus org.kde.KWin /KWin reconfigure
							check_error "status" "$?"
						sleep 2 #safety delay

						printf "${GC}\n*** STARTING ${CC}Picom${GC} in ${YC}TRANSPARENCY${GC} mode ***\n${NC}"
						picom --config ${PICOM} --backend glx --glx-fshader-win "$(cat ${GLSL})" & PID_PICOM=$!
						sleep 2 #safety delay
							printf "${GC}:::PID_PICOM=${YC}${PID_PICOM}${NC}\n"

						sleep 2 #safety delay
						printf "${GC}\n*** SETTING TRANSPARENCY PARAMETERS ***\n${NC}"
						picom-trans -w "${TACOwindow}" 99
							check_error "status" "$?"
						
						printf "${GC}\n*** REDIRECTING WINDOW FOCUS ***\n${NC}" 
						sleep 2 #safety delay
						xdotool behave ${TACOwindow} focus windowfocus ${GW2window} & PID_xdotool=$!
							printf "${GC}:::PID_xdotool=${YC}${PID_xdotool}${NC}\n"

					#------------- DISABLE -------------#
					elif [ "$action" == "stop" ]; then
					#------------- DISABLE -------------#

						printf "${GC}\n*** DISABLING THE ${CC}Picom${GC} COMPOSITOR ***${NC}\n"
						kill `pidof picom`
							check_error "status" "$?"
						
						printf "${GC}\n*** RESTARTING THE ${CC}KWin${GC} COMPOSITOR ***${NC}\n"
						qdbus org.kde.KWin /Compositor resume
							check_error "status" "$?"

					else
					exit 2
					fi # finish action

				#----------- OPACITY MODE ----------#
				elif [ "${MODE}" == "opacity" ]; then
				#----------- OPACITY MODE ----------#

					#------------ ACTIVATE ------------#
					if [ "$action" == "start" ]; then
					#------------ ACTIVATE ------------#

						printf "${GC}*** APPLYING ${CC}KWinRules${GC} WITH ${YC}active-opacity${GC} AT ${YC}${OPACITY}${GC} ***${NC}"
						kwriteconfig5 --file kwinrulesrc --group "$COUNT" --key opacityactive "${OPACITY}"
							check_error "status" "$?"
						
						printf "${GC}\n*** REFRESHING ${CC}KWin${GC} ***${NC}"
						qdbus org.kde.KWin /KWin reconfigure
							check_error "status" "$?"

					#------------- DISABLE -------------#
					elif [ "$action" == "stop" ]; then
					#------------- DISABLE -------------#

						printf "${GC}\n\v*** RESETTING ${CC}KWinRules${GC} ${YC}active-opacity${GC} ***${NC}"
						kwriteconfig5 --file kwinrulesrc --group "$COUNT" --key opacityactive "100"
							check_error "status" "$?"

						printf "${GC}\n*** REFRESHING ${CC}KWin${GC} ***${NC}"
						qdbus org.kde.KWin /KWin reconfigure
							check_error "status" "$?"

					else
					exit 2
					fi # finish action
				
				else
				exit 2
				fi # finish mode

			else
			exit 2
			fi # finish distribution

		;; #========================= END KDE ============================#

		Cinnamon) #================== CINNAMON ===========================#

			#------------------------------ DEBIAN & ARCH ------------------------------#
			if [ "$DIST" == "Debian/Ubuntu/Mint" ] || [ "$DIST" == "Arch/Manjaro" ]; then
			#------------------------------ DEBIAN & ARCH ------------------------------#

				#---------- TRANSPARENCY MODE ---------#
				if [ "${MODE}" == "transparency" ]; then
				#---------- TRANSPARENCY MODE ---------#

					#------------ ACTIVATE ------------#
					if [ "$action" == "start" ]; then
					#------------ ACTIVATE ------------#

						#DISABLE muffin composition
						printf "${GC}*** DISABLING COMPOSITION BY KILLING THE CINNAMON PROCESS ***${NC}\n"
						kill `pidof cinnamon`
							check_error "status" "$?"
						sleep 2 #safety delay
						
						printf "\n${GC}*** STARTING THE ${CC}openbox${GC} WINDOW MANAGER ***${NC}"
						openbox &
							printf "${GC}\n---STARTED!${NC}"
						sleep 2 #safety delay

						#START Picom composition
						printf "${GC}\n*** STARTING ${CC}Picom${GC} in ${YC}TRANSPARENCY${GC} mode ***${NC}\n"
						picom --config ${PICOM} --backend glx --glx-fshader-win "$(cat ${GLSL})" & PID_PICOM=$!
						sleep 2 #safety delay
							printf "${GC}:::PID_PICOM=${YC}${PID_PICOM}${NC}\n"

						sleep 2 #safety delay
						printf "${GC}\n*** SETTING TRANSPARENCY PARAMETERS ***\n${NC}"
						picom-trans -w "${TACOwindow}" 99
							check_error "status" "$?"
						
						#FIX WINDOW FOCUS
						printf "${GC}\n*** REDIRECTING WINDOW FOCUS ***\n${NC}" 
						sleep 2 #safety delay
						xdotool behave ${TACOwindow} focus windowfocus ${GW2window} & PID_xdotool=$!
							printf "${GC}:::PID_xdotool=${YC}${PID_xdotool}${NC}\n"

					#<<<---------- DISABLE ---------->>>#
					elif [ "$action" == "stop" ]; then
					#<<<---------- DISABLE ---------->>>#

						#DISABLE Picom
						printf "${GC}\n*** DISABLING PICOM ***${NC}"
						kill `pidof picom`
							check_error "status" "$?"
						
						#DISABLE Openbox
						printf "${GC}\n*** DISABLING OPENBOX ***${NC}"
						kill `pidof openbox`
							check_error "status" "$?"

						#START Cinnamon
						printf "${GC}\n*** RESTARTING CINNAMON ***${NC}"
						cinnamon &
						printf "${GC}\n---DONE.${NC}\n\v"

					else
					exit 2
					fi # finish action

				#----------- OPACITY MODE ----------#
				elif [ "${MODE}" == "opacity" ]; then
				#----------- OPACITY MODE ----------#

					#------------ ACTIVATE ------------#
					if [ "$action" == "start" ]; then
					#------------ ACTIVATE ------------#

						#DISABLE muffin composition
						printf "${GC}*** DISABLING COMPOSITION BY KILLING THE CINNAMON PROCESS ***${NC}"
						kill `pidof cinnamon`
							check_error "status" "$?"
						sleep 2 #safety delay

						printf "${GC}\n\v*** STARTING THE ${CC}openbox${GC} WINDOW MANAGER ***${NC}"
						openbox &
							printf "${GC}\n---STARTED!${NC}"

						printf "${GC}\n\v*** STARTING THE ${CC}Picom${GC} COMPOSITOR ***${NC}"
						picom --config ${PICOM} &
						
						printf "${GC}*** SETTING OPACITY PARAMETERS. ${YC}active-opacity${GC} IS AT ${YC}${OPACITY}${GC} ***${NC}"
						picom-trans -w "${GW2window}" ${OPACITY}
							check_error "status" "$?"

					#<<<---------- DISABLE ---------->>>#
					elif [ "$action" == "stop" ]; then
					#<<<---------- DISABLE ---------->>>#

						#DISABLE Picom
						printf "${GC}\n*** DISABLING PICOM ***${NC}"
						kill `pidof picom`
							check_error "status" "$?"
						
						#DISABLE Openbox
						printf "${GC}\n*** DISABLING OPENBOX ***${NC}"
						kill `pidof openbox`
							check_error "status" "$?"

						#START Cinnamon
						printf "${GC}\n*** RESTARTING CINNAMON ***${NC}"
						cinnamon &
						printf "${GC}\n---DONE.${NC}\n\v"

					else
					exit 2
					fi # finish action
				
				else
				exit 2
				fi # finish mode

			else
			exit 2
			fi # finish distribution

		;; #======================= END CINNAMON =========================#

		*) #==============================================================#
			printf "${RC}---ERROR: Failed to select DE${NC}";
			exit 2;
		;; #==============================================================#

	esac # END DE
}

taco_runner()
{
	#START TACO
	if [ "${WINE}" == "portable" ]; then
		"../../../bin/wine64" ./GW2TacO.exe & PID=$!

	elif [ "${WINE}" == "native" ]; then
		wine64 ./GW2TacO.exe & PID=$!

	elif [ "${WINE}" == "lutris" ]; then
		"${LUTW}/wine64" ./GW2TacO.exe & PID=$!

	fi
	printf "${GC}\n*** ${CC}TACO${GC} STARTED WITH PID ${YC}${PID}${GC} ***\n\v${NC}"

	#ACTIVATE TWEAKS
	sleep 10 #wait for taco
	do_tweaks "start"
	
	#WATCH TACO STATUS
	printf "${GC}\n\v*** WATCHING ${CC}TACO${GC} PROCESS FOR EXIT CODE ***${NC}\n\v"
	wait ${PID}
	printf "${GC}\n\v*** ${CC}TACO${GC} EXITED WITH CODE ${YC}[$?]${GC} ***${NC}\n\v"
	
	#DISABLE TWEAKS
	do_tweaks "stop"
}

check_start()
{

	#=================== NO ACTIVATE TACOPACK ================
	if [[ -z "${ACTIVE}" ]]; then # NOTIFY
		printf "${RC}*** !ERROR! No tacopacks set as active! ***\n\v${NC}"
		exit 0
	fi

	#======================== MODE ===========================
	if [[ "${MODE}" == "transparency" ]]; then # NOTIFY
		printf "${YC}*** !WARNING! You have enabled experimental transparency. Expect bugs & read the FAQ! ***\n\v${NC}"
	else
		printf "${YC}*** !NOTE! RUNNING IN OPACITY MODE! ***\n\v${NC}"
	fi

	#======================== WINE ===========================

		#------------------------ PORTABLE -------------------------#
		# AUTO START
		if [ "${WINE}" == "portable" ] && [ ${AUTO} == "true" ]; then

			if [ "$GW" == "gw2taco/app" ]; then # DISABLED
				printf "${RC}*** ${YC}AUTOSTART=TRUE${RC} BUT TRYING TO START MANUALLY. ***\n${GC}RUN GW2 OR SET ${YC}AUTOSTART=FALSE\n\v${NC}"

			else # START
				printf "${GC}*** GW2 STARTED. WAITING ${NC}${STA}s${GC} BEFORE STARTING TACO ***\n\v${NC}"
				cd "app/${ACTIVE}" && sleep ${STA}
				taco_runner
			fi

		fi

		# MANUAL START
		if [ "${WINE}" == "portable" ] && [ ${AUTO} == "false" ]; then

			if [ "$GW" == "drive_c/GW2" ]; then # DISABLE
				printf "${GC}*** ${YC}AUTOSTART=FALSE${GC}, SKIPPING TACO, USE ${CC}taco_runner.sh${GC} AFTER MAP LOADS***\n${NC}"

			else # START
				printf "${GC}*** MANUAL START, MAKE SURE GW2 IS RUNNING & MAP IS LOADED ***\n --> WAITING ${NC}${STM}s${GC} BEFORE STARTING TACO\n\v${NC}"
				export WINEPREFIX="${GDIR}/data"
				export WINEESYNC=1
				cd "app/${ACTIVE}" && sleep ${STM}
				taco_runner
			fi

		fi
		#------------------- END PORTABLE ------------------------#

		#------------------------ NATIVE -------------------------#
		# AUTO START
		if [ "${WINE}" == "native" ] && [ ${AUTO} == "true" ]; then

			printf "${GC}*** STARTING GW2... TACO WILL START in ${NC}${STA}s${GC} ***\n\v${NC}"
			../GW2.exe &
			cd "app/${ACTIVE}" && sleep ${STA}
			taco_runner

		fi

		# MANUAL START
		if [ "${WINE}" == "native" ] && [ ${AUTO} == "false" ]; then

			printf "${GC}*** MANUAL START, MAKE SURE GW2 IS RUNNING & MAP IS LOADED ***\n --> WAITING ${NC}${STM}s${GC} BEFORE STARTING TACO\n\v${NC}"
			#export WINEPREFIX="$HOME/.wine" #probably not needed
			#export WINEESYNC=1 
			cd "app/${ACTIVE}" && sleep ${STM}
			taco_runner

		fi
		#------------------------ END NATIVE -----------------------#

		#------------------------- LUTRIS ------------------------#
		# AUTO START
		if [ "${WINE}" == "lutris" ] && [ ${AUTO} == "true" ]; then
			printf "${GC}*** STARTING GW2 VIA LUTRIS... TACO WILL START in ${NC}${STA}s${GC} ***\n\v${NC}"
			lutris lutris:rungameid/${LUTG} &
			cd "app/${ACTIVE}" && sleep ${STA}
			taco_runner
		fi

		# MANUAL START
		if [ "${WINE}" == "lutris" ] && [ ${AUTO} == "false" ]; then
			printf "${GC}*** MANUAL START, MAKE SURE GW2 IS RUNNING & MAP IS LOADED ***\n --> WAITING ${NC}${STM}s${GC} BEFORE STARTING TACO\n\v${NC}"
			export WINEPREFIX="${LUTP}"
			export WINEESYNC=1
			cd "app/${ACTIVE}" && sleep ${STM}
			taco_runner

		fi
		#------------------------- END LUTRIS ---------------------#

	#======================= END WINE ========================

}

run_script() {
	#check_error()		#	Error Checks/Notices
	#do_xfwm4()		#	xfwm4 tweaks
	#do_tweaks()	#	Start/Stop tweaks
	#taco_runner()	#	Start/Stop Taco
	check_start 	#	precheck
}
run_script #initialize